<?php
    require_once 'api/configuration/config.php';

    /**
     * Класс работы методов с продуктами
     */
    class Products {
        /**
         * Имя таблицы в бд
         */
        private static $tableName = 'products';

        /**
         * Необходимые поля для заполнения
         */
        public static $Field = array('name','cost');

        /**
         * Получить все записи
         * @param SafeMySQL $db - класс работы с бд
         */
        public static function All($db){
            return Config::All($db, Products::$tableName);
        }

        /**
         * Получить запись по ее id
         * @param SafeMySQL $db - класс работы с бд
         * @param int $id - ид записи
        */
        public static function GetByID($db, int $id){
            return Config::GetByID($db, Products::$tableName, $id);
        }

        /**
        * Создание записи
        * @param SafeMySQL $db - класс работы с бд
        * @param array $data - данные для вставки array("name"=> "value")
        */
        public static function Into($db, $data){
            return Config::Into($db, Products::$tableName, $data);
        }

        /**
         * Обновление записи
         * @param SafeMySQL $db - класс работы с бд
         * @param array $data - данные для вставки array("name"=> "value")
         * @param int $id - обновляемый записи
         */
        public static function Update($db, $data, $id){
            return Config::Update($db, Products::$tableName, $data,$id);
        }

        /**
         * Удаление записи по $id
         * @param SafeMySQL $db - класс работы с бд
         * @param int $id - удаляемый записи
         */
        public static function DeleteById($db, $id){
            return Config::DeleteById($db,Products::$tableName,$id);
        }
    }
?>