<?php
        require_once 'api/configuration/safemysql.class.php';
        require_once 'api/api.php';
        require_once 'api/configuration/config.php';
        require_once 'Products.php';

        class ProductsApi extends Api
        {
            //Имя api Объекта
            public $Name = 'products';

            /**
            * Метод GET
            * Вывод списка всех записей
            * http://ДОМЕН/Объект
            * @return string
            */
            public function IndexAction(){
                $db = new SafeMySQL(Config::$opts);
                $product = Products::All($db);

                if($product){
                    return $this->Response($product,200);
                }
                return $this->Response("Data not found",404);
            }           

            /**
             * Метод GET
             * Просмотр отдельной записи (по id)
             * http://ДОМЕН/Объект/1
             * @return string
             */
            public function ViewAction(){
                $db = new SafeMySQL(Config::$opts);
                $id = array_shift($this->RequestUri);

                if($id){
                    $product = Products::GetByID($db,$id);
                    if($product) return $this->Response($product, 200);
                }
                return $this->Response("Product id=$id not found", 404);
            }

            /**
            * Метод POST
            * Создание новой записи
            * http://ДОМЕН/Объект + параметры запроса name, email
            * @return string
            */
            public function CreateAction(){
                $db = new SafeMySQL(Config::$opts);
                $data = $db->filterArray($this->RequestParams, Products::$Field);
                $noSetField = [];

                foreach (Products::$Field as $nameField) {
                    if(!isset($this->RequestParams[$nameField])) $noSetField[] = $nameField;
                }

                if(count($noSetField) > 0) return $this->Response("No set Field ".implode(",",$noSetField),500);
                $res = Products::Into($db,$data);
                return $this->Response($res, 200);
            }

            /**
             * Метод PUT
             * Обновление отдельной записи (по ее id)
             * http://ДОМЕН/Объект/1 + параметры запроса
             * @return string
             */
            public function UpdateAction(){
                $parse_url = parse_url($this->RequestUri[0]);
                $productId = $parse_url['path'] ?? null;

                $db = new SafeMySQL(Config::$opts);
                $data = $db->filterArray($this->RequestParams,Products::$Field);

                if(!$productId || !Products::GetByID($db, $productId)) {
                    return $this->Response("Product with id=$productId not found", 404);
                }
                
                $res = Products::Update($db, $data,$productId);
                if($res) return $this->Response("Data update", 200);
                return $this->Response("Update error", 400);
            }

                    
            /**
             * Метод DELETE
             * Удаление отдельной записи (по ее id)
             * http://ДОМЕН/Объект/1
             * @return string
             */
            public function DeleteAction(){
                $parse_url = parse_url($this->RequestUri[0]);
                $productId = $parse_url['path'] ?? null;
    
                $db = new SafeMySQL(Config::$opts);

                if(!$productId || !Products::GetByID($db, $productId)){
                    return $this->Response("Product with id=$productId not found", 404);
                }

                if(Products::DeleteById($db,$productId)){
                    return $this->Response("Data deleted", 200);
                }

                return $this->Response("Delete error", 500);
            }
        }
        
?>