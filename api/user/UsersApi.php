<?php 
    require_once 'api/configuration/safemysql.class.php';
    require_once 'api/api.php';
    require_once 'api/configuration/config.php';
    require_once 'Users.php';

    class UsersApi extends Api
    {
        public $Name = 'users';

        /**
         * Метод GET
         * Вывод списка всех записей
         * http://ДОМЕН/users
         * @return string
         */
        public function IndexAction(){
            $db = new SafeMySQL(Config::$opts);
            $users = Users::All($db);

            if($users){
                return $this->Response($users,200);
            }
            return $this->Response("Data not found",404);
        }

        /**
         * Метод GET
         * Просмотр отдельной записи (по id)
         * http://ДОМЕН/users/1
         * @return string
         */
        public function ViewAction(){
            $db = new SafeMySQL(Config::$opts);

            $id = array_shift($this->RequestUri);
			$user = null;

            if(is_numeric($id)){
                $user = Users::GetByID($db,$id);
            }else if($id){
				$user = Users::GetByName($db,$id);
			}
			
			if($user){
                return $this->Response($user, 200);
            }
			
            return $this->Response("User not found",404);
        }

        /**
         * Метод POST
         * Создание новой записи
         * http://ДОМЕН/users + параметры запроса name, email
         * @return string
         */
        public function CreateAction(){
            $db = new SafeMySQL(Config::$opts);
            $data = $db->filterArray($this->RequestParams,Users::$Field);
            $noSetField = [];

            foreach (Users::$Field as $nameField) {
                if(!isset($this->RequestParams[$nameField])) $noSetField[] = $nameField;
            }

            if(count($noSetField) > 0) return $this->Response("No set Field ".implode(",",$noSetField),500);

            $res = Users::Into($db,$data);
            return $this->Response($res,200);
        }

        /**
         * Метод PUT
         * Обновление отдельной записи (по ее id)
         * http://ДОМЕН/users/1 + параметры запроса name, email
         * @return string
         */
        public function UpdateAction(){
			
			if(!isset($this->RequestUri[0])) return $this->Response("No set id paramter",500);
			
            $parse_url = parse_url($this->RequestUri[0]);
            $userId = $parse_url['path'] ?? null;
			
			if(empty($userId)) return $this->Response("No set id paramter",500);

            $db = new SafeMySQL(Config::$opts);
            $data = $db->filterArray($this->RequestParams,Users::$Field);
			
			if(count($data) == 0) return $this->Response("No set Field update",500);

            if(!$userId || !Users::GetByID($db, $userId)){
                return $this->Response("User with id=$userId not found", 404);
            }
            
            $res = Users::Update($db, $data, $userId);
            if($res) return $this->Response("Data update", 200);
            return $this->Response("Update error", 400);
        }

        /**
         * Метод DELETE
         * Удаление отдельной записи (по ее id)
         * http://ДОМЕН/users/1
         * @return string
         */
        public function DeleteAction(){
			if(!isset($this->RequestUri[0])) return $this->Response("No set id delete",500);
			
            $parse_url = parse_url($this->RequestUri[0]);
            $userId = $parse_url['path'] ?? null;

            $db = new SafeMySQL(Config::$opts);
            
            if(!$userId || !Users::GetByID($db, $userId)){
                return $this->Response("User with id=$userId not found", 404);
            }

            if(Users::DeleteById($db, $userId)){
                return $this->Response('Data deleted.', 200);
            }
            return $this->Response("Delete error", 500);
        }
    }
