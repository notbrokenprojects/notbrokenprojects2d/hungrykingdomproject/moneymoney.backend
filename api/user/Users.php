<?php
    require_once 'api/configuration/config.php';

     /**
     * Класс работы методов с пользователями
     */
    class Users {
        /**
         * Имя таблицы в бд
         */
        private static $tableName = 'users';

        /**
         * Необходимые поля для заполнения
         */
        public static $Field = array('name','money','rate');

        /**
        * Получить все записи
        * @param SafeMySQL $db - класс работы с бд
        */
        public static function All($db){
            return Config::All($db, Users::$tableName);
        }
		
		public static function GetByName($db, string $name){
			return Config::GetByName($db,Users::$tableName,$name);
		}

        /**
         * Получить запись по ее id
         * @param SafeMySQL $db - класс работы с бд
         * @param int $id - ид записи
        */
        public static function GetByID($db, int $id){
            return Config::GetByID($db, Users::$tableName, $id);
        }

        /**
        * Создание записи
        * @param SafeMySQL $db - класс работы с бд
        * @param array $data - данные для вставки array("name"=> "value")
        */
        public static function Into($db, $data){
            return Config::Into($db, Users::$tableName, $data);
        }

        /**
         * Обновление записи
         * @param SafeMySQL $db - класс работы с бд
         * @param array $data - данные для вставки array("name"=> "value")
         * @param int $id - обновляемый записи
         */
        public static function Update($db, $data, $id){
            return Config::Update($db, Users::$tableName, $data,$id);
        }

        /**
         * Удаление записи по $id
         * @param SafeMySQL $db - класс работы с бд
         * @param int $id - удаляемый записи
         */
        public static function DeleteById($db, $id){
            return Config::DeleteById($db,Users::$tableName,$id);
        }
    }
?>