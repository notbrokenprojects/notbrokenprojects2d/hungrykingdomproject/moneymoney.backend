<?php
    abstract class Api
    {
        //Наименование api объекта
        public $Name = '';   

        //Методы GET, POST, PUT, DELETE
        protected $method = '';

        //Url - запроса
        public $RequestUri = [];

        //Параметры запроса
        public $RequestParams = [];

        //Наименование действия
        protected $action = '';

        /**
         * Конструктор
         */
        public function __construct(){
            header("Access-Control-Allow-Orgin: *");
            header("Access-Control-Allow-Methods: *");
            header("Content-Type: application/json");
    
            //Массив GET параметров разделенных слешем
            $this->RequestUri = explode('/', trim($_SERVER['REQUEST_URI'],'/'));
            $this->RequestParams = $_REQUEST;
    
            //Определение метода запроса
            $this->method = $_SERVER['REQUEST_METHOD'];

            if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
                if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                    $this->method = 'DELETE';
                } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                    $this->method = 'PUT';
                } else {
                    throw new Exception("Unexpected Header");
                }
            }            
        }

        /**
         * Запуск Api
         */
        public function Run() {			
            //Первые 2 элемента массива URI должны быть "api" и название таблицы
            if(array_shift($this->RequestUri) !== 'api' || array_shift($this->RequestUri) !== $this->Name){
                throw new RuntimeException('API Not Found', 404);
            }
            //Определение действия для обработки
            $this->action = $this->GetAction();
    
            //Если метод(действие) определен в дочернем классе API
            if (method_exists($this, $this->action)) {
                return $this->{$this->action}();
            } else {
                throw new RuntimeException('Invalid Method', 405);
            }
        }

        /**
         * Формирования ответа
         */
        protected function Response($data, $status = 500) {
            header("HTTP/1.1 " . $status . " " . $this->RequestStatus($status));
            return json_encode($data);
        }

        /**
         * Получение сообщения для статуса по коду
         */
        private function RequestStatus($code) {
            $status = array(
                200 => 'OK',
                404 => 'Not Found',
                405 => 'Method Not Allowed',
                500 => 'Internal Server Error',
            );
            return ($status[$code])?$status[$code]:$status[500];
        }
		
		//Вернуть ошибку
		protected function GetError($message){
			return array("error"=> $message);
		}

        /**
         * Возращает наименование действия в зависимости
         * от метода запроса
         */
        protected function GetAction()
        {
            $method = $this->method;
            switch ($method) {
                case 'GET':
                    if($this->RequestUri){
                        return 'ViewAction';
                    } else {
                        return 'IndexAction';
                    }
                    break;
                case 'POST':
                    return 'CreateAction';
                    break;
                case 'PUT':
                    return 'UpdateAction';
                    break;
                case 'DELETE':
                    return 'DeleteAction';
                    break;
                default:
                    return null;
            }
        }

        abstract protected function IndexAction();
        abstract protected function ViewAction();
        abstract protected function CreateAction();
        abstract protected function UpdateAction();
        abstract protected function DeleteAction();
    }    
?>