<?
    require_once 'api/configuration/config.php';

     /**
     * Класс работы методов с закупками
     */
    class Purchases
    {
        /**
        * Имя таблицы в бд
        */
        private static $tableName = 'purchases';

        /**
        * Необходимые поля для заполнения opt = опцилнальные (необязательные)
        */
        public static $Field = array('productId','userId','amount', 'opt'=>'liked');

        /**
         * Получить все записи
         * @param SafeMySQL $db - класс работы с бд
         */
        public static function All($db){
            return Config::All($db, Purchases::$tableName);
        }

        /**
         * Получить запись по ее id
         * @param SafeMySQL $db - класс работы с бд
         * @param int $id - ид записи
        */
        public static function GetByID($db, int $id){
            return Config::GetByID($db, Purchases::$tableName, $id);
        }

         /**
         * Получить запись по ее id продукт/пользователь
         * @param SafeMySQL $db - класс работы с бд
         * @param int $id - ид записи
        */
        public static function GetByUserProduct($db, $id, $userId){
            $where = $db->parse('productId = ?i and userId = ?i', $id, $userId);
            return Config::GetOneByWhere($db,Purchases::$tableName, $where);
        }

        /**
        * Создание записи
        * @param SafeMySQL $db - класс работы с бд
        * @param array $data - данные для вставки array("name"=> "value")
        */
        public static function Into($db, $data){
            return Config::Into($db, Purchases::$tableName, $data);
        }

        /**
         * Обновление записи
         * @param SafeMySQL $db - класс работы с бд
         * @param array $data - данные для вставки array("name"=> "value")
         * @param int $id - обновляемый записи
         */
        public static function Update($db, $data, $id){
            return Config::Update($db, Purchases::$tableName, $data,$id);
        }

        /**
         * Удаление записи по $id
         * @param SafeMySQL $db - класс работы с бд
         * @param int $id - удаляемый записи
         */
        public static function DeleteById($db, $id){
            return Config::DeleteById($db,Purchases::$tableName,$id);
        }
    }
?>