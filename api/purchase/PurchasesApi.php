<?
    require_once 'api/configuration/safemysql.class.php';
    require_once 'api/api.php';
    require_once 'api/configuration/config.php';
    require_once 'Purchases.php';

    class PurchasesApi extends Api {

        //Имя api Объекта
        public $Name = 'purchases';

        /**
         * Метод GET
         * Вывод списка всех записей
         * http://ДОМЕН/Объект
         * @return string
         */
        public function IndexAction(){
            $db = new SafeMySQL(Config::$opts);
            $purchases = Purchases::All($db);

            if($purchases){
                return $this->Response($purchases,200);
            }
            return $this->Response("Data not found",404);
        }

        /**
         * Метод GET
         * Просмотр отдельной записи (по id) или (по id продукт/пользователь)
         * http://ДОМЕН/Объект/1 или http://Домен/Объект/1/1
         * @return string
         */
        public function ViewAction(){
            $db = new SafeMySQL(Config::$opts);

            $id = array_shift($this->RequestUri);
            $userId = array_shift($this->RequestUri);

            if($id){
                $purchase = !$userId ? Purchases::GetByID($db,$id) : Purchases::GetByUserProduct($db,$id, $userId);
                if($purchase){
                    return $this->Response($purchase, 200);
                }
            }
            return $this->Response("Purchases not found",404);
        }

        /**
         * Метод POST
         * Создание новой записи
         * http://ДОМЕН/Объект + параметры запроса name, email
         * @return string
         */
        public function CreateAction(){
            $db = new SafeMySQL(Config::$opts);
            $data = $db->filterArray($this->RequestParams,Purchases::$Field);
            $noSetField = [];

            foreach (Purchases::$Field as $key => $nameField) {
                if(strpos($key,'opt') !== false) continue;
                if(!isset($this->RequestParams[$nameField])) $noSetField[] = $nameField;
            }

            if(count($noSetField) > 0) return $this->Response("No set Field ".implode(",",$noSetField),500);

            $res = Purchases::Into($db,$data);
            return $this->Response($res,200);
        }

        /**
         * Метод PUT
         * Обновление отдельной записи (по ее id) или (по id продукт/пользователь)
         * http://ДОМЕН/Объект/1 + параметры запроса http://ДОМЕН/Объект/1/2 + параметры запроса
         * @return string
         */
        public function UpdateAction(){
			
			if(!isset($this->RequestUri[0])) return $this->Response("Set record id or id product/user", 500);
			
            $parse_url = parse_url($this->RequestUri[0]);
            $parse_url2 = isset($this->RequestUri[1]) ? parse_url($this->RequestUri[1]) : null;
            $id = $parse_url['path'] ?? null;
            $userId = $parse_url2['path'] ?? null;

            $db = new SafeMySQL(Config::$opts);
            $data = $db->filterArray($this->RequestParams,Purchases::$Field);

            if(count($data) == 0) return $this->Response("No set params", 500);

            if($id && $userId){
                $purchase = Purchases::GetByUserProduct($db, $id, $userId);
                if(!$purchase) return $this->Response("Purchase with productId=$id, userId=$userId not found");
                $id = $purchase["id"];
            }elseif (!$id || !Purchases::GetByID($db, $id)){
                return $this->Response("Purchase with id=$id not found", 404);
            }
            
            $res = Purchases::Update($db, $data, $id);
            if($res) return $this->Response("Data update", 200);
            return $this->Response("Update error", 500);
        }

        /**
         * Метод DELETE
         * Удаление отдельной записи (по ее id)
         * http://ДОМЕН/Объект/1
         * @return string
         */
        public function DeleteAction(){
			if(!isset($this->RequestUri[0])) return $this->Response("Set record id or id product/user", 500);
            $parse_url = parse_url($this->RequestUri[0]);
            $parse_url2 = isset($this->RequestUri[1]) ? parse_url($this->RequestUri[1]) : null;

            $id = $parse_url['path'] ?? null;
            $userId = $parse_url2['path'] ?? null;

            $db = new SafeMySQL(Config::$opts);
            
            if($id && $userId){
                $purchase = Purchases::GetByUserProduct($db,$id,$userId);				
                if(!$purchase) return $this->Response("Purchase with productId=$id, userId=$userId not found"); 
                $id = $purchase["id"];
            }elseif(!$id || !Purchases::GetByID($db, $id)){
                return $this->Response("Purchase with id=$id not found", 404);
            }

            if(Purchases::DeleteById($db, $id)){
                return $this->Response('Data deleted.', 200);
            }
            return $this->Response("Delete error", 500);
        }
    }
?>