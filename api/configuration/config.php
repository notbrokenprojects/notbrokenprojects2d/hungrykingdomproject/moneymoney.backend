<?php

/**
 * Класс конфигурации работы с БД
 */
class Config
{
    /**
     * Опции подключения
     */
    public static  $opts = array(
        'host'      => 'localhost',
		'user'    => 'root',
		'pass'    => 'root',
		'db'      => 'fantom',
		'charset' => 'utf8'
	);   

    /**
     * Показать все записи таблицы
     * @param SafeMySQL $db - класс работающией с бд
     * @param string $tableName - Имя таблицы
     */
    public static function All($db, string $tableName){
        return $db->getAll("SELECT * FROM ?n", $tableName);
    }

    /**
     * Показать записи по условию
     * @param SafeMySQL $db - класс работающей с бд
     * @param string $tableName - Имя таблицы
     * @param string $query - Условие выборки 
     */
    public static function AllWhere($db, $tableName, $query){
        $query = 'WHERE '.$query;
        return $db->getAll("SELECT * FROM ?n ?p",$tableName,$query);
    }

    /**
     * Вернуть одну запись по условию
     * @param SafeMySQL $db - класс работающией с бд
     * @param string $tableName - Имя таблицы
     * @param string $query - Условие выборки
     */
    public static function GetOneByWhere($db, $tableName, $query)
    {
        $query = 'WHERE '.$query;
        return $db->getRow("SELECT * FROM ?n ?p", $tableName, $query);
    }

    /**
     * Показать запись по id
     * @param SafeMySQL $db - класс работающией с бд
     * @param string $tableName - Имя таблицы
     * @param int $id - Ид запсиа
     */
    public static function GetByID($db,string $tableName,int $id){
        return $db->getRow("SELECT * FROM ?n WHERE id = ?i", $tableName, $id);
    }

    /**
     * Вставка записи
     * @param SafeMySQL $db - класс работающией с бд
     * @param string $tableName - Имя таблицы
     * @param Params $data - Данные для вставки
     */
    public static function Into($db, $tableName,$data){
        try{
            $res = $db->query("INSERT INTO ?n SET ?u", $tableName, $data);
            $id = $db->insertId();
            return $id;
        }catch(\Exception $ex){
            return $ex->getMessage();
        }
    }

    /**
     * Обновить запись
     * @param SafeMySQL $db - класс работающией с бд
     * @param string $tableName - Имя таблицы
     * @param Params $data - Данные обновления
     * @param int $id - ид обновляемый записи
     */
    public static function Update($db,$tableName, $data, $id){
        $res = $db->query("UPDATE ?n SET ?u WHERE id = ?i", $tableName, $data, $id);
        return $res;
    }

    /**
     * Удаление записи
     * @param SafeMySQL $db - класс работающией с бд
     * @param string $tableName - Имя таблицы
     * @param int $id - ид записи
     */
    public static function DeleteById($db, $tableName, $id){
        return $db->query("DELETE FROM ?n WHERE id = ?i", $tableName,$id);
    }
}
