<?php
	require_once 'api/configuration/safemysql.class.php';
	require_once 'api/api.php';
	require_once 'api/configuration/config.php';
	require_once 'Foods.php';
	
	class FoodsApi extends Api {
		
		//Имя api объекта 
		public $Name = 'foods';
		
		/**
		* Метод GET
		* Вывод списка всех записей
		* http://ДОМЕН/Объект
		* @return string
		*/
		public function IndexAction(){
			$db = new SafeMySQL(Config::$opts);
			$foods = Foods::All($db);
			
			if($foods) return $this->Response($foods,200);
			return $this->Response("Foods not found",404);
		}
		
		/**
		* Метод GET
		* Просмотр отдельной записи (по id)
		* http://ДОМЕН/Объект/1
		* @return string
		*/
		public function ViewAction(){
			$db = new SafeMySQL(Config::$opts);
			$id = array_shift($this->RequestUri);

			if($id){
				$food = Foods::GetByID($db,$id);
				if($food) return $this->Response($food, 200);
			}
			return $this->Response("Food id=$id not found", 404);
		}
		
		/**
		* Метод POST
		* Создание новой записи
		* http://ДОМЕН/Объект + параметры запроса name, email
		* @return string
		*/
		public function CreateAction(){
			$db = new SafeMySQL(Config::$opts);
			$data = $db->filterArray($this->RequestParams, Foods::$Field);
			$noSetField = [];

			foreach (Foods::$Field as $nameField) {
				if(!isset($this->RequestParams[$nameField])) $noSetField[] = $nameField;
			}

			if(count($noSetField) > 0) return $this->Response("No set Field ".implode(",",$noSetField),500);
			$res = Foods::Into($db,$data);
			return $this->Response($res, 200);
		}
		
		/**
		* Метод PUT
		* Обновление отдельной записи (по ее id)
		* http://ДОМЕН/Объект/1 + параметры запроса
		* @return string
		*/
		public function UpdateAction(){
			$parse_url = parse_url($this->RequestUri[0]);
			$foodId = $parse_url['path'] ?? null;

			$db = new SafeMySQL(Config::$opts);
			$data = $db->filterArray($this->RequestParams,Foods::$Field);
			
			if(!$data) return $this->Response('No set param update: '.implode(' or ', Foods::$Field) ,500);

			if(!$foodId || !Foods::GetByID($db, $foodId)) {
				return $this->Response("Food with id=$foodId not found", 404);
			}

			$res = Foods::Update($db, $data,$foodId);
			if($res) return $this->Response("Data update", 200);
			return $this->Response("Update error", 400);
		}
		
		/**
		 * Метод DELETE
		 * Удаление отдельной записи (по ее id)
		 * http://ДОМЕН/Объект/1
		 * @return string
		 */
		public function DeleteAction(){
			$parse_url = parse_url($this->RequestUri[0]);
			$foodId = $parse_url['path'] ?? null;

			$db = new SafeMySQL(Config::$opts);
			$data = $db->filterArray($this->RequestParams,Foods::$Field);

			if(!$foodId || !Foods::GetByID($db, $foodId)) {
				return $this->Response("Food with id=$foodId not found", 404);
			}

			if(Foods::DeleteById($db,$foodId)){
				return $this->Response("Data deleted", 200);
			}

			return $this->Response("Delete error", 500);
		}		
	}
?>