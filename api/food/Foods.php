<?php
	require_once 'api/configuration/config.php';
	
	class Foods {
		
		/**
         * Имя таблицы в бд
         */
        private static $tableName = 'foods';

        /**
         * Необходимые поля для заполнения
         */
        public static $Field = array('name','price','calories');

        /**
         * Получить все записи
         * @param SafeMySQL $db - класс работы с бд
         */
        public static function All($db){
            return Config::All($db, Foods::$tableName);
        }

        /**
         * Получить запись по ее id
         * @param SafeMySQL $db - класс работы с бд
         * @param int $id - ид записи
        */
        public static function GetByID($db, int $id){
            return Config::GetByID($db, Foods::$tableName, $id);
        }

        /**
        * Создание записи
        * @param SafeMySQL $db - класс работы с бд
        * @param array $data - данные для вставки array("name"=> "value")
        */
        public static function Into($db, array $data){
            return Config::Into($db, Foods::$tableName, $data);
        }

        /**
         * Обновление записи
         * @param SafeMySQL $db - класс работы с бд
         * @param array $data - данные для вставки array("name"=> "value")
         * @param int $id - обновляемый записи
         */
        public static function Update($db, array $data, int $id){
            return Config::Update($db, Foods::$tableName, $data,$id);
        }

        /**
         * Удаление записи по $id
         * @param SafeMySQL $db - класс работы с бд
         * @param int $id - удаляемый записи
         */
        public static function DeleteById($db, int $id){
            return Config::DeleteById($db,Foods::$tableName,$id);
        }
	}
?>