<?php
    require_once 'api/product/ProductsApi.php';
    require_once 'api/user/UsersApi.php';
    require_once 'api/purchase/PurchasesApi.php';
	require_once 'api/food/FoodsApi.php';

    //Опеределяем к какому api объекту обратится
	$api = null;
	$path = parse_url($_SERVER["REQUEST_URI"]);
    $url = explode("/",$path['path']);
    if($url[1] == 'api'){
        switch($url[2]){
            case 'users':
                $api = new UsersApi();
                break;
            case 'products':
                $api = new ProductsApi();
                break;
            case 'purchases':
                $api = new PurchasesApi();
                break;
			case 'foods':
				$api = new FoodsApi();
				break;
			break;
            default:
				header("HTTP/1.1 405 Method Not Allowed");
                echo "Api no support method '$url[2]'";
                exit();
                break;
        }

        echo $api->Run();
    }
?>